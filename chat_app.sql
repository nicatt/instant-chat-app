-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2019 at 06:02 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `password`, `email`, `register_date`) VALUES
(1, 'nicat', '123456', 'hi@nicat.org', '2019-04-13 19:57:31'),
(2, 'senan', '123456', 'hi@senan.org', '2019-04-13 19:57:31'),
(3, 'admin', '123456', 'hi@admin.org', '2019-04-13 19:57:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;



--Chat table start
CREATE TABLE `chat` (
    `message_id` INT NOT NULL AUTO_INCREMENT ,
    `sender_id` INT NOT NULL ,  `receiver_id` INT NOT NULL ,
    `message` VARCHAR(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL ,
    `sent_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY  (`message_id`)
    ) ENGINE = InnoDB;
--Chat end

--Communities table start
CREATE TABLE `communities` (
    `community_id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(50) NOT NULL ,
    `title` VARCHAR(100) NOT NULL ,
    `created_by` INT NOT NULL ,
    `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY  (`community_id`), UNIQUE  (`name`)
    ) ENGINE = InnoDB;
--Community end

--Rooms table start
CREATE TABLE `rooms` (
    `room_id` INT NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(50) NOT NULL ,
    `title` VARCHAR(100) NOT NULL ,
    `created_by` INT NOT NULL ,
    `create_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY  (`room_id`),    UNIQUE  (`name`)
    ) ENGINE = InnoDB;
--Rooms table end


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
