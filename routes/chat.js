'use strict';

module.exports = (io) => {
    const hbs = require('hbs');
    const express = require('express');
    const xss = require('xss');
    const Database = require('../model/chat');
    const UploadImage = require('../helpers/UploadFile');

    const router = express.Router();
    const preventXSS = str => {
        return new xss.FilterXSS({
            whiteList: {
                b: [],
                i: [],
                small: [],
                mark: [],
                del: [],
                ins: [],
                sub: []
            }
        }).process(str);
    };
    const db = new Database();

    let usocket = {};
    let socketList = [];

    io.on('connection', socket => {
        //region Initialize
        socket.on('init', r => usocket[socket.handshake.session.user.id] = socket);
        //endregion

        //region Send Message
        socket.on('send private message', res => {
            res.from = socket.handshake.session.user.id;
            res.msg = preventXSS(res.msg.substring(0, 500));

            //fayl
            if (res.file) {
                let upload = new UploadImage(res.file);
                upload.result();
            }

            //Mesajın DB-ya yazılması
            db.sendMessage(res.from, res.to, res.msg)
                .catch(e => console.log(e)) //TODO: err log
                .then(response => {
                    //response true-dursa və receiver online-dırsa emit olunsun
                    if (response.affectedRows > 0 && typeof usocket[res.to] !== 'undefined') {
                        // usocket[res.to].emit('receive private message', res);
                        usocket[res.to].emit('receive private message', res);
                    }
                });
        });
        //endregion

        //region Typing
        socket.on('SEND_TYPING_STATUS', id => {
            id = id.toString();

            if (typeof usocket[id] !== 'undefined') {
                usocket[id].emit('RECEIVE_TYPING_STATUS', socket.handshake.session.user.id);
            }
        });
        //endregion

        //region Diconnect
        socket.on('disconnect', function () {
            console.log('Got disconnect!');

            //disconnect olursa session id olmur ona görə də server crash olur
            // delete usocket[socket.handshake.session.user.id];

            let index = socketList.indexOf(socket.id);
            if (index > -1) {
                socketList.splice(index, 1);
            }
        });
        //endregion
    });

    router.get('/', function (req, res, next) {
        //sayta daxil olmamış linkə giribsə, qayıtsın geriyə
        if (!req.session.user) {
            res.redirect("/");
            return;
        }

        io.set('authorization', function (handshake, callback) {
            handshake.user = req.session.user;
            callback(null, true);
        });

        db.users(req.session.user.id)
            .catch(e => console.log(e)) //TODO: err log
            .then(data => {
                if(data) {
                    let chat = {};

                    for (let i = 0; i < data.length; i++) {
                        chat[data[i].user_id] = data[i].name;
                    }

                    hbs.registerHelper('json', function (context) {
                        return JSON.stringify(context);
                    });

                    res.render('chat', {
                        me: req.session.user,
                        users: data,
                        chat: chat
                    });
                }
                else console.log('err'); //TODO: err log
            });
    });

    return router;
};
