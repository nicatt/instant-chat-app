'use strict';

const express = require('express');
const database = require('../model/sign-up');
const router = express.Router();

router.get('/', function(req, res, next) {
    if(res.session && res.session.user){
        res.redirect("/");
        return;
    }

    res.render('sign-up/index');
});

router.post('/', function(req, res, next) {
    if(res.session && res.session.user){
        res.redirect("/");
        return;
    }

    const util = require('../helpers/Utils');
    const db = new database();

    let isOK = util.checkParams(req, ['username', 'pass', 'email']);
    function err() {
        res.render('sign-up/index');
    }

    if(!isOK){
        err();
        return false;
    }

    db.signUp(req.body.username, req.body.pass, req.body.email)
        .catch((e) => console.log(e)) //TODO: err log
        .then((v) => {
            if(v && v.affectedRows > 0) res.redirect("/");
            else err();
        });
});


module.exports = router;