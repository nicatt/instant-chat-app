'use strict';

const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    if (req.session.user) res.redirect("/chat");
    else res.render('index');
});

router.post('/login', function(req, res, next) {
    const database = require('../model/user');
    const db = new database();

    db.login(req.body.username, req.body.pass)
        .catch(e => console.log(e)) //TODO: err log
        .then(data =>{
            if (data && data[0]) {
                data = data[0];
                req.session.user = {id: data.user_id, name: data.name, hash: data.hash};

                res.redirect("/chat");
            }
            else res.redirect("/");
        })
});

module.exports = router;