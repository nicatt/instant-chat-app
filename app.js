'use strict';

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session')({
    name: 'sessionName',
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 60 * 1000 * 60 * 24,
        resave: false,
        saveUninitialized: true,
        //secure: true //FIXME: SECURITY production-a keçəndə bunu aç ki, HTTPS protokoli ilə işləsin
    }
});
const logger = require('morgan');
const sharedsession = require("express-socket.io-session");

let app = express();

app.io = require('socket.io')();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('trust proxy', 1); // trust first proxy

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session);
app.io.use(sharedsession(session, {
    autoSave:true
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/index'));
app.use('/sign-up', require('./routes/sign-up'));
app.use('/chat', require('./routes/chat')(app.io));


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
