'use strict';

const Connection = require('./database');

module.exports = class Chat extends Connection{
    constructor(){
        super();
    }

    async users(me) {
        let self = this;
        let sql = 'SELECT user_id, name FROM users WHERE user_id != ?;';

        let result = await self.query(sql, [me]);

        return result;
    }

    async sendMessage(sender, receiver, message) {
        let self = this;
        let sql = "INSERT INTO `chat` (`sender_id`, `receiver_id`, `message`) VALUES (?, ?, ?);";

        let result = await self.query(sql, [sender, receiver, message]);

        return result;
    }
};
