'use strict';

let Connection = require('./database');

module.exports = class SingUp extends Connection{
    constructor(){
        super();
    }

    async signUp(name, pass, email) {
        let self = this;
        let sql = 'INSERT INTO `users` (`name`, `password`, `email`) VALUES (?, ?, ?);';
        //FIXME: hash yaratmır, şifrəni adi string kimi saxlayır

        let result = await self.query(sql, [name, pass, email]);

        return result;
    }
};
