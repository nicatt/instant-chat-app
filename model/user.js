'use strict';

const Connection = require('./database');

module.exports = class User extends Connection{
    constructor(){
        super();
    }

    async login(name, password) {
        let self = this;
        let sql = 'SELECT user_id, name FROM users WHERE name=? AND password=?;';

        let result = await self.query(sql, [name, password]);

        return result;
    }
};
