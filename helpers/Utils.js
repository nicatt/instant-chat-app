'use strict';

const Utils = {
    checkParams: (req, params) => {
        for (let index in params){
            if(typeof req.body[params[index]] === 'undefined'){
                return false;
            }
        }

        return true;
    }
};

module.exports = Utils;