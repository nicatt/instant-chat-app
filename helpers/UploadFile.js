'use strict';

const fs = require('fs');
const sha1 = require('sha1');
const mime = require('mime-types');

module.exports = class UploadFile {

    //this.$mimeTypes
    //this.$mime
    //this.$fileName

    constructor(base64) {
        this.$mimeTypes = ["jpeg", "jpg", "png", "gif", "bmp"];
        let match = base64.match(/^data:image\/(jpeg|jpg|png|gif|bmp);base64,(.+)$/);

        this.$mime = match && match.length ? match["1"] : 'invalid';
        let now  = Date.now();

        this.$fileName = "uploads/images/"+sha1("kakash"+base64+now)+"."+this.$mime;

        this.upload(base64);
    }

    upload(base64){
        if(this.$mimeTypes.indexOf(this.$mime) > -1){
            let start = "data:image/"+this.$mime+";base64,";
            base64 = base64.substring(start.length);

            fs.writeFile(this.$fileName, base64, 'base64', err => console.log(err));
        }
    }

    validate(){
        let mime = mime.lookup(this.$fileName).substring(6); //`image/` hissəsinin silinməsi

        return mime = this.$mime;
    }

    result(){
        let validate = this.validate();

        if(validate) return validate;
        else{

            fs.unlink(this.$fileName, e => {});

            return false;
        }
    }
};